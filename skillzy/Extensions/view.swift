//
//  view.swift
//  newart
//
//  Created by MACBOOK on 7/15/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import FCAlertView
extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
}
public func makeErrorAlert(subTitle: String) {
    let alert = FCAlertView()
    alert.makeAlertTypeWarning()
    alert.colorScheme = ColorControl.sharedInstance.getMainColor()
    alert.showAlert(withTitle: "Error".localized(), withSubtitle: subTitle, withCustomImage: nil, withDoneButtonTitle: "Ok".localized(), andButtons: nil)
}
public func makeSuccessAlert(subTitle: String) {
    let alert = FCAlertView()
    alert.makeAlertTypeWarning()
    alert.colorScheme = ColorControl.sharedInstance.getMainColor()
    alert.showAlert(withTitle: "Done".localized(), withSubtitle: subTitle, withCustomImage: nil, withDoneButtonTitle: "Ok".localized(), andButtons: nil)
}
public func generateQRCode(from string: String, frame:CGRect) -> UIImage? {
    let data = string.data(using: String.Encoding.utf8)
    if let filter = CIFilter(name: "CIQRCodeGenerator") {
        guard let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }
        
        filter.setValue(data, forKey: "inputMessage")
        
        filter.setValue("H", forKey: "inputCorrectionLevel")
        colorFilter.setValue(filter.outputImage, forKey: "inputImage")
        colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1") // Background white
        colorFilter.setValue(CIColor(color: #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)), forKey: "inputColor0") // Foreground or the barcode RED
        guard let qrCodeImage = colorFilter.outputImage
            else {
                return nil
        }

        let scaleX = frame.size.width / qrCodeImage.extent.size.width
        let scaleY = frame.size.height / qrCodeImage.extent.size.height
        let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
 
        
        if let output = colorFilter.outputImage?.transformed(by: transform) {
            return UIImage(ciImage: output)
        }
    }
    return nil}
extension UIColor {
    var coreImageColor: CIColor {
        return CIColor(color: self)
    }
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let color = coreImageColor
        return (color.red, color.green, color.blue, color.alpha)
    }
}

extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
extension UIView {
    
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    func addShadow(shadowColor: CGColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1),
                   shadowOffset: CGSize = CGSize(width: -1, height: 1.0),
                   shadowOpacity: Float = 0.5,
                   shadowRadius: CGFloat = 5.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
        
    }

}
extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 71
        return sizeThatFits
    }
}
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[kCTForegroundColorAttributeName as NSAttributedStringKey: newValue!])
        }
    }
}
public extension UIWindow {
    
    /** @return Returns the current Top Most ViewController in hierarchy.   */
    public func topMostVC()->UIViewController? {
        
        var topController = rootViewController
        
        while let presentedController = topController?.presentedViewController {
            topController = presentedController
        }
        
        return topController
    }
    
    /** @return Returns the topViewController in stack of topMostController.    */
    public func currentViewController()->UIViewController? {
        
        var currentViewController = topMostVC()
        
        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }
        
        return currentViewController
    }
}
