//
//  CustomSwitch.swift
//  skillzy
//  Created by Mohamed Nawar on 8/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//
import UIKit
@IBDesignable

class UISwitchCustom: UISwitch {
    @IBInspectable var OffTint: UIColor? {
        didSet {
            self.tintColor = OffTint
            self.layer.cornerRadius = 16
            self.backgroundColor = OffTint
        }
    }
}
