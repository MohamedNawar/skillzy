//
//  MainTabBarController.swift
//  skillzy
//  Created by Mohamed Nawar on 8/26/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit


class MainTabBar: UITabBar {

    private var middleButton = UIButton()
    private var middleTitle = UILabel()


    override func awakeFromNib() {
        super.awakeFromNib()
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "NeoSansArabic", size: 13)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "NeoSansArabic", size: 13)!], for: .selected)
        setupMiddleButton()
    }
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 50 + safeAreaInsets.bottom
        return sizeThatFits
    }


    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.isHidden {
            return super.hitTest(point, with: event)
        }
        
        let from = point
        let to = middleButton.center

        return sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)) <= 39 ? middleButton : super.hitTest(point, with: event)
    }

    func setupMiddleButton() {
        middleButton.frame.size = CGSize(width: 55, height: 55)
        middleButton.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
        let image = UIImage(named: "qr11")
        middleButton.setImage(image, for: .normal)
        middleButton.layer.cornerRadius = 27.5
        middleButton.layer.masksToBounds = true
        middleButton.center = CGPoint(x: (UIScreen.main.bounds.width / 2) , y: 0)
        middleButton.addTarget(self, action: #selector(test), for: .touchUpInside)
        addSubview(middleButton)
        middleTitle.frame.size = CGSize(width: 55, height: 20)
        middleTitle.textColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
        middleTitle.text = "Add".localized()
        middleTitle.tag = -1
        middleTitle.textAlignment = .center
        middleTitle.font = UIFont(name: "NeoSansW23-Medium", size: 12)
        middleTitle.center = CGPoint(x: UIScreen.main.bounds.width / 2 , y: 42)

        addSubview(middleTitle)
        bringSubview(toFront: middleTitle)
    }

    var BtnPressed : (()->())?
    @objc func test() {
        BtnPressed?()
    }
}
