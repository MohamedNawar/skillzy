//
//  MarkerView.swift
//  skillzy
//
//  Created by Moaz Ezz on 9/15/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PulsingHalo

class MarkerView: UIView {
    let halo = PulsingHaloLayer()
    override func awakeFromNib() {
        let halo = PulsingHaloLayer()
        halo.instanceCount = 6
        halo.radius = self.frame.width/2
        //        halo.duration = 7.0
        halo.backgroundColor = ColorControl.sharedInstance.getMainColor().cgColor
        halo.position = self.center
        self.layer.addSublayer(halo)
        halo.start()
    }
    
}
