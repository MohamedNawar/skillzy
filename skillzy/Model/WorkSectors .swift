//
//  WorkSectors .swift
//  skillzy
//
//  Created by MACBOOK on 9/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation
import Alamofire
class WorkSectorAndPosition {
    static let Instance = WorkSectorAndPosition()
    var workSector = WorkSectorsData()
    var position = PositionData()
    private init() {}
    
    public func getWorkSectorNames() -> [String]{
        var temp = [String]()
        for item in (WorkSectorAndPosition.Instance.workSector.data){
            temp.append(item.name ?? "")
        }
        return temp
        
    }
    
    
    public func getWorkSectors(enterDoStuff: @escaping () -> Void)  {
        
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.getWorkSectors(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    
                    do {
                        
                        self.workSector = try JSONDecoder().decode(WorkSectorsData.self, from: response.data!)
                    }catch{
                        
                    }
                }
                enterDoStuff()
            case .failure( _):
                enterDoStuff()
            }
        }
        
        
    }
    public func getPositionsNames() -> [String]{
        var temp = [String]()
        for item in (WorkSectorAndPosition.Instance.position.data){
            temp.append(item.name ?? "")
        }
        return temp
        
    }
    
    
    public func getPositions(enterDoStuff: @escaping () -> Void)  {
        
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.getPositions(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            switch(response.result) {
            case .success(let value):
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    
                    do {
                        
                        self.position = try JSONDecoder().decode(PositionData.self, from: response.data!)
                    }catch{
                        
                    }
                }
                enterDoStuff()
            case .failure(let error):
                enterDoStuff()
            }
        }
        
        
    }

    
    
}

struct WorkSectorsData : Decodable {
    var data = [WorkSectorsDataDetails]()
}

struct WorkSectorsDataDetails : Decodable {
    var id : Int?
    var name : String?
}
struct PositionData : Decodable {
    var data = [PositionDataDetails]()
}

struct PositionDataDetails : Decodable {
    var id : Int?
    var name : String?
}

