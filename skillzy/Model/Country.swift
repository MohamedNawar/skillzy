//
//  Country.swift
//  skillzy
//
//  Created by Mohamed Nawar on 9/2/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation
import Alamofire

class CountryMain {
  static let Instance = CountryMain()
    var countryData = Country()
    private init() {}
    
    public func getCountryNameArr() -> [String]{
        var temp = [String]()
        for item in (CountryMain.Instance.countryData.data ){
            temp.append(item.name ?? "")
        }
        return temp

    }
   
    
    public func getCountriesServer(enterDoStuff: @escaping () -> Void)  {
        
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.getCountries(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    
                    do {
                        
                           self.countryData = try JSONDecoder().decode(Country.self, from: response.data!)
                    }catch{
                        
                    }
                }
                enterDoStuff()
            case .failure( _):
                enterDoStuff()
            }
        }
        
        
    }


    
}
class Country : Decodable {
    var data = [CountryData]()
    }

class CountryData : Decodable {
    func getCitiesName() -> [String] {
        var temp = [String]()
        for item in self.cities ?? [Cities]() {
            temp.append(item.name ?? "")
        }
        return temp
    }
    var id : Int?
    var name : String?
    var cities : [Cities]?
}
class Cities : Decodable {
    var id : Int?
    var name : String?
}
