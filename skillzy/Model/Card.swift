//
//  Card.swift
//  skillzy
//
//  Created by Mohamed Nawar on 8/28/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation

struct Card : Decodable {
    var data: CardData?
}
struct CardArr:Decodable {
    var data: [CardData]?
}
struct CardData : Decodable {
    var id : Int?
    var distance_formatted:String?
    var name : String?
    var brief : String?
    var avatar : String?
    var gender : String?
    var city_id : Int?
    var position_id : Int?
    var work_sector_id : Int?
    var company : String?
    var phone : String?
    var mobile : String?
    var address : String?
    var email : String?
    var social_media : SOCIAL_PROVIDER?
    var is_default : Bool?
    var user: User?
    var city: CityData?
    var position: Position?
    var work_sector : Work_sector?
    var location_latitude: Double?
    var location_longitude : Double?
    var share : String?
    // var links:Links?
    var links: UncertainValue<[String], Links>
//    private enum CodingKeys: String, CodingKey {
//        case name
//        case id
//        case brief
//        case avatar
//        case gender
//        case city_id
//        case position_id
//        case work_sector_id
//        case company
//        case phone
//        case mobile
//        case address
//        case email
//        case social_media
//        case is_default
//        case user
//        case city
//        case position
//        case work_sector
//        case location_latitude
//        case location_longitude
//        case share
//
//    }
}

public struct UncertainValue<T: Decodable, U: Decodable>: Decodable {
    public var tValue: T?
    public var uValue: U?
    
    public var value: Any? {
        return tValue ?? uValue
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        tValue = try? container.decode(T.self)
        uValue = try? container.decode(U.self)
        if tValue == nil && uValue == nil {
            //Type mismatch
            throw DecodingError.typeMismatch(type(of: self), DecodingError.Context(codingPath: [], debugDescription: "The value is not of type \(T.self) and not even \(U.self)"))
        }
        
    }
}


struct Links : Decodable {
    var remove_from_contacts:LinkDetails?
    //    enum CodingKeys: String, CodingKey {
    //        case UrgentServices = "urgent-services"
    //    }
}
struct LinkDetails : Decodable {
    var href:String?
    var method:String?
}



struct SOCIAL_PROVIDER : Decodable, Encodable{
    var website : String?
    var instagram : String?
    var facebook : String?
    var snapchat : String?
    var twitter : String?
    var linkedin : String?
}
struct User : Decodable{
    var id : Int?
    var name : String?
    var email : String?
    var is_listed : Bool?
    
}
struct CityData : Decodable{
    var id : Int?
    var name : String?
    var country: CountryDataInfo?
}
struct CountryDataInfo : Decodable{
    var id : Int?
    var name : String?
}
struct Position : Decodable{
    var id : Int?
    var name : String?
}
struct Work_sector : Decodable{
    var id : Int?
    var name : String?
}


