//
//  UserData.swift
//  skillzy
//
//  Created by Mohamed Nawar on 8/28/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import Foundation
import UIKit

struct userData : Decodable {
    static var Instance = userData()
    private init() {
        fetchUser()
    }
    var data : UserDetails?
    var message : String?
    var token : String?
    
    func saveUser(data:Data) {
        
        UserDefaults.standard.set(data, forKey: "user")
        
        
    }
    
    mutating func remove() {
        UIApplication.shared.unregisterForRemoteNotifications()
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.set(0, forKey: "addedCard")
        UserDefaults.standard.set("", forKey: "updatedEmail")
        UserDefaults.standard.set("", forKey: "isListed")
        token = ""
    }
    
    mutating func fetchUser(){
        if let  data = UserDefaults.standard.data(forKey: "user") {
            do {
                self = try JSONDecoder().decode(userData.self, from: data)
            }catch{
                print("hey check me out!!")
            }
        }
    }
    
    
    func isLogged()->Bool {
        if token == "" || token == nil {
            return false
        }
        return true
    }
    
}

struct UserDetails : Decodable {
    var email : String?
    var id : Int?
    var name : String?
    var is_listed : Bool?
    var cards_count : Int?
}




struct PhotoFormat : Decodable {
    var id : Int?
    var large : String?
    var medium : String?
    var original : String?
    var thumb : String?
    
}
struct link : Decodable {
    var edit : String?
}

struct Edit : Decodable {
    var edit : String?
    var editPassword : String?
    var href : String?
}
