//
//  AppDelegate.swift
//  skillzy
//
//  Created by MACBOOK on 8/9/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import OneSignal
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OSSubscriptionObserver {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        L102Localizer.DoTheMagic()
        IQKeyboardManager.shared.enable = true
        NotificationCenter.default.addObserver(self, selector: #selector(pushToCard), name: NSNotification.Name(rawValue: "PushToCard"), object: nil)
        GMSServices.provideAPIKey("AIzaSyB5bqJ6WM0DJcdmNdHnVc_ZQRO2GVb9AV4")
        if #available(iOS 11.0, *) {
            UINavigationBar.appearance().largeTitleTextAttributes = [
                NSAttributedStringKey.font: UIFont(name: "NeoSansW23-Medium", size: 37) ?? UIFont.systemFontSize]
        }
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 0.1882352941, green: 0.1960784314, blue: 0.1882352941, alpha: 1)
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "59d7e73f-a6a8-4ccb-b3ad-b8695fde35cd",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        OneSignal.add(self as OSSubscriptionObserver)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    @objc func pushToCard(_ notification: NSNotification) {
        
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if userData.Instance.isLogged() {
            print("URL:::\(url)")
            print("url host :\(url.host as String!)")
            print("url path :\(url.path as String!)")
            if let id = url.path as? String {
                let filteredId = id.replacingOccurrences(of: "/", with: "")
                print(filteredId)
                let vc = AppStoryboard.Main.viewController(viewControllerClass: CardInnerVC.self)
                vc.showDismissButton = true
                vc.cardId = filteredId
                let navVC = UINavigationController.init(rootViewController: vc)
                navVC.modalPresentationStyle = .overFullScreen
                UIApplication.shared.keyWindow?.currentViewController()?.show(navVC, sender: self)
            }
        }
        return true
        
    }
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "onesignalid")
            print(UserDefaults.standard.string(forKey: "onesignalid"))
            print("Current playerId \(playerId)")
            //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addOneSignalIdNC"), object: nil, userInfo: nil)
            
        }
    }
    
}

