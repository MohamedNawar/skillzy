//
//  SettingViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/15/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
class SettingViewController: UIViewController,LanguageDelegate,UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    func didSelectLang(lang: String) {
        if lang == L102Language.currentAppleLanguage() {
            return
        }
        L102Language.setAppleLAnguageTo(lang: lang)
        if lang == "ar"{
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        };        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "uitabViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    
    @IBOutlet var publicSwitch: UISwitchCustom!
    
    @IBAction func switchChanged(_ sender: Any) {
        postListUpdate()
    }
    var help = ""
    var about = ""
    @IBAction func editButtPressed(_ sender: Any) {
        let vc = AppStoryboard.SecondSB.viewController(viewControllerClass: ProfileVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func changeLang(_ sender: Any) {
        let langVC =  AppStoryboard.Main.viewController(viewControllerClass: changeLanguageVC.self)
        langVC.delegate = self
        langVC.modalPresentationStyle = .overFullScreen
        self.present(langVC, animated: true, completion: nil)
    }
    
    
    @IBAction func aboutApp(_ sender: Any) {
        guard let url = URL(string: about) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func heelpButt(_ sender: Any) {
        guard let url = URL(string: help) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        userData.Instance.remove()
        let loginVC =  storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        let navLogin = UINavigationController.init(rootViewController: loginVC ?? UIViewController())
        navLogin.modalPresentationStyle = .overFullScreen
        self.present(navLogin, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       scrollView.delegate = self
       
      
        scrollView.showsHorizontalScrollIndicator = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.string(forKey: "isListed") == nil || UserDefaults.standard.string(forKey: "isListed") == "" {
            publicSwitch.isOn = userData.Instance.data?.is_listed ?? true
        }else{
            if UserDefaults.standard.string(forKey: "isListed") == "true" {
                publicSwitch.isOn = true
            }else{
                publicSwitch.isOn = false
            }
            
        }
        getSettings()
    }
    func postListUpdate(){
        HUD.show(.progress)
        let par =
        ["is_listed": publicSwitch.isOn]
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.updateProfile() , method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let value):
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    if self.publicSwitch.isOn {
                        UserDefaults.standard.set("true", forKey: "isListed")
                    }else{
                        UserDefaults.standard.set("false", forKey: "isListed")
                    }
                }
                
            case .failure( _):
                break
            }
        }
    }
    func getSettings(){
        HUD.show(.progress)
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.settings() , method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let value):
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    if let data = value as? [String:AnyObject] {
                        if let dataDict = data["data"] as? [String:AnyObject] {
                            if let generalDict = dataDict["general"] as? [String:AnyObject] {
                                self.about = "\(generalDict["about_app"] ?? "" as AnyObject)"
                                self.help = "\(generalDict["help"] ?? "" as AnyObject)"
                            }
                            if let userDict = dataDict["user"] as? [String:Bool] {
                                self.publicSwitch.isOn = userDict["is_listed"] ?? true
                            }
                        }
                    }
                }
                
            case .failure( _):
                break
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }

    
}
