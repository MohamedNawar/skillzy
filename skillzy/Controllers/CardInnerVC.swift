//
//  CardInnerVC.swift
//  skillzy
//
//  Created by mouhammed ali on 9/9/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
class CardInnerVC: UIViewController {
    var card:CardData?
    var isSearching = false
    var cardId = ""
    @IBOutlet var editButt: UIButton!
    
    @IBOutlet var linkedinLbl: UILabel!
    @IBOutlet var linkedInView: UIView!
    var showDismissButton = false
    @IBOutlet var scrollView: UIScrollView!
    @IBAction func editPressed(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: EnterDataViewController.self)
        vc.isEditing = true
        vc.card.data = card
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Reload"), object: nil)
        }
    }
    @IBOutlet var workSectorLbl: UILabel!
    @IBOutlet var instagramView: UIView!
    @IBOutlet var instagramLbl: UILabel!
    @IBOutlet var snapchatView: UIView!
    @IBOutlet var snapchatLbl: UILabel!
    @IBOutlet var facebookLbl: UILabel!
    @IBOutlet var facebookView: UIView!
    @IBOutlet var twitterLbl: UILabel!
    @IBOutlet var twitterView: UIView!
    @IBOutlet var emailView: UIView!
    @IBOutlet var emailLbl: UILabel!
    @IBOutlet var webLbl: UILabel!
    @IBOutlet var webView: UIView!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var mobilLbl: UILabel!
    @IBOutlet var telephoneLbl: UILabel!
    @IBOutlet var comp2Lbl: UILabel!
    @IBOutlet var jobPosition: UILabel!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var compLbl: UILabel!
    @IBOutlet var cityLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var qrCodeImg: UIImageView!
    
    @IBOutlet var addButtonBack: UIButton!
    @IBAction func addButtonPressed(_ sender: Any) {
        print(addButton.currentTitle)
        if addButtonBack.currentTitle == "Remove -".localized() {
            addOrRemove(type:"remove")
        }else{
            addOrRemove(type:"add")
        }
    }
    
    @IBOutlet var addButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCard), name: NSNotification.Name(rawValue: "ReloadInner"), object: nil)
        if cardId == "" {
            parseCard()}
        else{
            getCardsData()
        }
        if !showDismissButton {
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: ""), for: .default)
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    func parseCard(){
        qrCodeImg.image = generateQRCode(from: card?.share ?? "", frame: qrCodeImg.frame)
        descLbl.sizeToFit()
        self.view.layoutIfNeeded()
        if card?.user?.id == userData.Instance.data?.id {
            addButtonBack.isHidden = true
            addButton.isHidden = true
            editButt.isHidden = false
        }
        if let links = card?.links.value as? Links {
            if links.remove_from_contacts?.href != nil {
                addButtonBack.setTitle("Remove -".localized(), for: .normal)
            }else{
                addButtonBack.setTitle("Add +".localized(), for: .normal)
            }
        }
        workSectorLbl.text = card?.work_sector?.name ?? ""
        nameLbl.text = card?.name ?? ""
        cityLbl.text = "\(card?.city?.name ?? ""), \(card?.city?.country?.name ?? "")"
        compLbl.text = card?.company ?? ""
        descLbl.text = card?.brief ?? ""
        jobPosition.text = card?.position?.name ?? ""
        comp2Lbl.text = card?.work_sector?.name ?? ""
        telephoneLbl.text = card?.phone ?? ""
        mobilLbl.text = card?.mobile ?? ""
        locationLbl.text = card?.address ?? ""
        webLbl.text = card?.social_media?.website ?? ""
        emailLbl.text = card?.email ?? ""
        facebookLbl.text = card?.social_media?.facebook ?? ""
        instagramLbl.text = card?.social_media?.instagram ?? ""
        twitterLbl.text = card?.social_media?.twitter ?? ""
        snapchatLbl.text = card?.social_media?.snapchat ?? ""
        linkedinLbl.text = card?.social_media?.linkedin ?? ""
        if emailLbl.text == "" {
            emailView.isHidden = true
        }
        if webLbl.text == "" {
            webView.isHidden = true
        }
        if facebookLbl.text == "" {
            facebookView.isHidden = true
        }
        if twitterLbl.text == "" {
            twitterView.isHidden = true
        }
        if snapchatLbl.text == "" {
            snapchatView.isHidden = true
        }
        if instagramLbl.text == "" {
            instagramView.isHidden = true
        }
        if linkedinLbl.text == "" {
            linkedInView.isHidden = true
        }
    }
    func addOrRemove(type:String){
        let url = "http://app.skillzyconnect.com/api/cards/contacts/\(card?.id ?? -1)"
        var methode = HTTPMethod.post
        if type == "remove" {
            methode = .delete
        }
        HUD.show(.progress)
        let header = APIs.Instance.getHeader()
        Alamofire.request(url, method: methode, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let value):
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    if (self.navigationController?.viewControllers.count ?? 0) > 1 {
                        if let parentVC = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2]{
                            print(parentVC.className)
                            if let parentVC = parentVC as? TabeViewController {
                                parentVC.secondViewController?.reload = true
                                parentVC.secondViewController?.getMyCads(hideHud: false)
                                self.navigationController?.popViewController(animated: true)
                                
                            }
                            else if let parentVC = parentVC as? RandomCardsVC {
                                
                                
                                parentVC.kolodaView.swipe(.left)
                                
                                self.navigationController?.popViewController(animated: true)
                            }else if let parent = parentVC as? SearchResultsVC {
                                parent.getResults()
                                self.navigationController?.popViewController(animated: true)
                            }else if let parent = parentVC as? NearByViewController {
                                parent.getNearBy()
                                self.navigationController?.popViewController(animated: true)
                            }else if let _ = parentVC as? MainMapVC{
                                self.navigationController?.popViewController(animated: true)
                            }else{
                                self.finishAdding()
                            }
                        }
                        
                    }else{
                        self.finishAdding()
                    }
                }
                
            case .failure( _): break
                
            }
        }
        
        
    }
    func getCardsData()  {
        let header = APIs.Instance.getHeader()
        HUD.show(.progress)
        var url = ""
        if let _ = Int(cardId) {
            url = APIs.Instance.getShowCard(id:cardId)
        }else{
            url = cardId.replacingOccurrences(of: "http://app.skillzyconnect.com/cards/", with: "http://app.skillzyconnect.com/api/cards/")
        }
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let value):
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    
                    do {
                        let CardMain = try JSONDecoder().decode(Card.self, from: response.data!)
                        self.card = CardMain.data
                        self.parseCard()
                        
                    }catch{
                        print(error)
                    }
                }
            case .failure( _):
                print("error")
            }
        }
        
        
    }
    func finishAdding(){
        if addButtonBack.currentTitle == "Remove -".localized() {
            addButtonBack.setTitle("Add +".localized(), for: .normal)
            makeSuccessAlert(subTitle: "Card was removed successfully".localized())
        }else{
            addButtonBack.setTitle("Remove -".localized(), for: .normal)
            makeSuccessAlert(subTitle: "Card was added successfully".localized())
        }
    }
    @objc func reloadCard(){
        cardId = "\(card?.id ?? 0)"
        getCardsData()
    }
    
}
