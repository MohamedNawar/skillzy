//
//  SearchViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/11/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//



import UIKit
import TextFieldEffects
import Font_Awesome_Swift
import PKHUD
class SearchViewController: UIViewController ,UIPickerViewDelegate , UIPickerViewDataSource  {
    
    @IBOutlet var randomCardBtn: UIButton!
    @IBOutlet var nameField: DesignableTextField!
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    let picker3 = UIPickerView()
    let picker4 = UIPickerView()
    var selectedCountry = CountryData()
    var cityId :Int = -1
    var workSectorId :Int = -1
    var positionId :Int = -1
    var selectedGender = ""
    var words = [""]
    var selected = ""
    var words1 = [""]
    var selected1 = ""
    let words2 = ["male", "female"]
    var selected2 = ""
    var words3 = [""]
    var selected3 = ""
    var words4 = [""]
    var selected4 = ""

    @IBAction func searchPreseed(_ sender: Any) {
        var url = APIs.Instance.getSearchForCard() + "?name=\(nameField.text ?? "")&gender=\(selectedGender)"
        
        if selectedCountry.id != nil {
            url = url + "&country_id=\(selectedCountry.id ?? 0)"
        }
        if cityId != -1 {
            url = url + "&city_id=\(cityId)"
        }
        if positionId != -1 {
            url = url + "&position_id=\(positionId)"
        }
        if workSectorId != -1 {
            url = url + "&work_sector_id=\(workSectorId)"
        }
        let vc = AppStoryboard.Main.viewController(viewControllerClass: SearchResultsVC.self)
        vc.url = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var chooseWorkSector: UITextField!
    @IBOutlet weak var specializationTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var countryTxtField: UITextField!
    
    
    
    @IBAction func cardsButtonPressed(_ sender: Any) {
        let vc = AppStoryboard.SecondSB.viewController(viewControllerClass: RandomCardsVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            randomCardBtn.setImage(#imageLiteral(resourceName: "ic_shuffle_reversed"), for: .normal)
        }
        countryTxtField.attributedPlaceholder = NSAttributedString(string: countryTxtField.placeholder ?? "",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        pickerInitialize()
        showspecializationPicker()
        showCountryPicker()
        showCityPicker()
        genderPicker()
        chooseWorkSectorPicker()
        loadCountryData()
        cityTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        countryTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        specializationTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        genderTextField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        chooseWorkSector.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)

        cityTxtField.addPadding(UITextField.PaddingSide.left(20))
        countryTxtField.addPadding(UITextField.PaddingSide.left(20))
        specializationTextField.addPadding(UITextField.PaddingSide.left(20))
        genderTextField.addPadding(UITextField.PaddingSide.left(20))
        chooseWorkSector.addPadding(UITextField.PaddingSide.left(20))

    }
   
    func pickerInitialize() {
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker1.delegate = self
        picker1.dataSource = self
        picker1.backgroundColor = .white
        picker1.showsSelectionIndicator = true
        picker2.delegate = self
        picker2.dataSource = self
        picker2.backgroundColor = .white
        picker2.showsSelectionIndicator = true
        picker3.delegate = self
        picker3.dataSource = self
        picker3.backgroundColor = .white
        picker3.showsSelectionIndicator = true
        picker4.delegate = self
        picker4.dataSource = self
        picker4.backgroundColor = .white
        picker4.showsSelectionIndicator = true

    }
    
    func showCountryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        countryTxtField.inputView = picker
        countryTxtField.inputAccessoryView = toolbar
        
    }
    func showspecializationPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done4Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        specializationTextField.inputView = picker4
        specializationTextField.inputAccessoryView = toolbar
        
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done1Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTxtField.inputView = picker1
        cityTxtField.inputAccessoryView = toolbar
        
    }
    
    func genderPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done2Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        genderTextField.inputView = picker2
        genderTextField.inputAccessoryView = toolbar
        
    }
    func chooseWorkSectorPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done3Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        chooseWorkSector.inputView = picker3
        chooseWorkSector.inputAccessoryView = toolbar
        
    }
    
    @objc func donePicker(){
        if countryTxtField.text == "" {
            if words.count > 0 {
                countryTxtField.text = words[0]
                selectedCountry = CountryMain.Instance.countryData.data[0]
                words1 = selectedCountry.getCitiesName()
                self.picker1.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        cityTxtField.isEnabled = true
        
    }
    @objc func done1Picker(){
        if cityTxtField.text == "" {
            if words1.count > 0 {
                cityTxtField.text = words1[0]
                cityId =  selectedCountry.cities?[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func done2Picker(){
        if genderTextField.text == "" {
            if words2.count > 0 {
                selectedGender = words2[0]
                genderTextField.text = words2[0].localized()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func done4Picker(){
        if specializationTextField.text == "" {
            if words3.count > 0 {
                specializationTextField.text = words4[0]
                positionId = WorkSectorAndPosition.Instance.position.data[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func done3Picker(){
        if chooseWorkSector.text == "" {
            if words4.count > 0 {
                chooseWorkSector.text = words3[0]
                workSectorId =  WorkSectorAndPosition.Instance.workSector.data[0].id ?? 0
                
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker {
            return words.count
        }
        if pickerView == picker1 {
            return words1.count
        }
        if pickerView == picker2 {
            return words2.count
        }
        if pickerView == picker3 {
            return words3.count
        }
        if pickerView == picker4 {
            return words4.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker {
            return "\(words[row])"
            
        }
        if pickerView == picker1 {
            return "\(words1[row])"
        }
        if pickerView == picker2 {
            return "\(words2[row].localized())"
        }
        if pickerView == picker3 {
            return "\(words3[row])"
        }
        if pickerView == picker4 {
            return "\(words4[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker {
            if words.count > 0 {
                selectedCountry = CountryMain.Instance.countryData.data[row]
                words1 = selectedCountry.getCitiesName()
                countryTxtField.text =  words[row]
                self.picker1.reloadAllComponents()
                cityId = -1
                cityTxtField.text = ""
            }  else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == picker1 {
            if words1.count > 0 {
                selected1 = words1[row]
                cityId =  selectedCountry.cities?[row].id ?? 0
                cityTxtField.text = selected1
            }else{
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        if pickerView == picker2 {
            if words2.count > 0 {
                selectedGender = words2[row]
                genderTextField.text = words2[row].localized()
                
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == picker3 {
            chooseWorkSector.text = words3[row]
            workSectorId =  WorkSectorAndPosition.Instance.workSector.data[row].id ?? 0
        }
        if pickerView == picker4 {
            if words4.count > 0 {
                selected4 = words4[row]
                
                positionId =  WorkSectorAndPosition.Instance.position.data[row].id ?? 0
                specializationTextField.text = selected4
            }else{
                self.dismiss(animated: true, completion: nil)
            }
            
        }

    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .white
    }
    func loadCountryData() {
        HUD.show(.progress)
        userData.Instance.fetchUser()
        CountryMain.Instance.getCountriesServer(enterDoStuff: { () in
            self.words = CountryMain.Instance.getCountryNameArr()
            self.picker.reloadAllComponents()
            HUD.hide()
        })
        WorkSectorAndPosition.Instance.getWorkSectors(enterDoStuff: { () in
            self.words3 = WorkSectorAndPosition.Instance.getWorkSectorNames()
            self.picker3.reloadAllComponents()
            HUD.hide()
        })
        WorkSectorAndPosition.Instance.getPositions(enterDoStuff: { () in
            self.words4 = WorkSectorAndPosition.Instance.getPositionsNames()
            self.picker4.reloadAllComponents()
            HUD.hide()
        })
        
    }
    
}
