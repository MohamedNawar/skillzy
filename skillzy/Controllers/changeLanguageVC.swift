//
//  changeLanguageVC.swift
//  skillzy
//
//  Created by mouhammed ali on 9/16/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class changeLanguageVC: UIViewController {
    var delegate:LanguageDelegate!
    @IBAction func changeToEnglish(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.delegate.didSelectLang(lang: "en")
        }
    }
    
    @IBAction func changeTiArabic(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate.didSelectLang(lang: "ar")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
protocol LanguageDelegate: NSObjectProtocol{
    func didSelectLang(lang: String)
}
