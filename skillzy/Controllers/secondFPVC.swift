//
//  secondFPVC.swift
//  Rafeeq
//
//  Created by mouhammed ali on 6/8/18.
//  Copyright © 2018 mouhammed ali. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
class secondFPVC: UIViewController {
    var email = ""
    
    
    @IBAction func submitButt(_ sender: Any) {
        if textField.text != "" {
            submitFunc()
        }else{
            showMissingFieldMsg()
        }
    }
    @IBOutlet var textField: UITextField!
    override func viewDidLoad() {
        self.navigationController?.hero.isEnabled = true
        self.view.viewWithTag(2)?.hero.id = "forgetView"
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    func submitFunc(){
        let par = ["email":email,
                   "code":textField.text!]
        print(par)
        HUD.show(.progress)
        Alamofire.request("http://app.skillzyconnect.com/api/auth/password/check-code", method: .post, parameters: par, encoding: URLEncoding.default, headers: APIs.Instance.getHeader()).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
                
                
            case .success(let value):
                // print(value)
                
                
                print(value)
                if let dict = value as? [String:AnyObject] {
                    if let tokenRec = dict["token"] as? String {
                        let vc =  AppStoryboard.Main.viewController(viewControllerClass: ThirdFPVC.self)
                        vc.token = tokenRec
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    if let errors = dict["errors"] as? [String:AnyObject] {
                        var flag = 0
                        for value in errors.values {
                            if flag == 0 {
                                flag = 1
                                
                                if let strArr = value as? [String] {
                                    if strArr.count>0 {
                                        self.showError(msg:strArr[0], titleMsg: "error")}
                                }
                            }
                            break
                        }
                    }}
            case .failure(let error):
                print(error)
                
            }
        }
    }
    func showMissingFieldMsg(){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString("Error", comment: ""), withSubtitle: NSLocalizedString("Please fill in the received code", comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("OK", comment: ""), andButtons: nil)
    }
    func showError(msg:String, titleMsg:String){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString(titleMsg, comment: ""), withSubtitle: NSLocalizedString(msg, comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("Ok", comment: ""), andButtons: nil)
        
    }
    
    
    
}
