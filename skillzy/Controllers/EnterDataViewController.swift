//
//  EnterDataViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/12/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Font_Awesome_Swift
import UITextView_Placeholder
import PKHUD
import Alamofire

class EnterDataViewController: UIViewController , UITextViewDelegate ,UIPickerViewDelegate , UIPickerViewDataSource , UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    var edit = false
    
    @IBOutlet var topLblHeight: NSLayoutConstraint!
    @IBOutlet var topViewNumberHeight: NSLayoutConstraint!
    @IBOutlet var nextButt: UIButton!
    @IBOutlet var logoutButt: UIButton!
    var selectedGender = ""
    let picker = UIPickerView()
    let picker1 = UIPickerView()
    let picker2 = UIPickerView()
    let picker3 = UIPickerView()
    let picker4 = UIPickerView()
    var alertController = UIAlertController()
    let imagePicker = UIImagePickerController()
    var selectedCountry = CountryData()
    var card = Card()
    var jsonData = Data()
    var words = [""]
    var selected = ""
    var words1 = [""]
    var selected1 = ""
    let words2 = ["male", "female"]
    var selected2 = ""
    var words3 = [""]
    var selected3 = ""
    var words4 = [""]
    var selected4 = ""
    var cityId :Int = 0
    var workSectorId :Int = 0
    var positionId :Int = 0
    var yourCardId = 0
    var imageFlag = -1
    @IBAction func backPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet var backButton: UIButton!
    
    @IBAction func logoutPressed(_ sender: UIButton) {
        if sender.currentTitle == "Back".localized() {
            self.dismiss(animated: true, completion: nil)
            return
        }
        userData.Instance.remove()
        self.navigationController?.popViewController(animated: true)
    }
    
    

    @IBOutlet var linkedInField: UITextField!
    @IBOutlet weak var companyTxtField: UITextField!
    @IBOutlet weak var addressTxtField: DesignableTextField!
    @IBOutlet weak var mobileTxtField: DesignableTextField!
    @IBOutlet weak var phoneTxtField: DesignableTextField!
    @IBOutlet weak var nameTxtField: DesignableTextField!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var snapchatTxtField: UITextField!
    @IBOutlet weak var instgramTxtField: UITextField!
    @IBOutlet weak var twitterTxtField: UITextField!
    @IBOutlet weak var WebsiteTxtField: UITextField!
    @IBOutlet weak var facebookTxtField: UITextField!
    @IBOutlet weak var jobDataTxtFiled: UITextField!
    @IBOutlet weak var jobTxtField: UITextField!
    @IBOutlet weak var genderTxtField: UITextField!
    @IBOutlet weak var cityTxtField: UITextField!
    @IBOutlet weak var countryTxtField: UITextField!
    @IBOutlet weak var DetailsAboutYouTextView: UITextView!
    @IBOutlet var emailField: DesignableTextField!
    
    @IBOutlet weak var counterLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if L102Language.isRTL {
            backButton.setImage(#imageLiteral(resourceName: "left-arrow"), for: .normal)
        }
        if isEditing {
            topLblHeight.constant = 0
            topViewNumberHeight.constant = 0
            nextButt.setTitle("Update".localized(), for: .normal)
            backButton.isHidden = false
            self.view.layoutIfNeeded()
            parseCard()
        }
        if UserDefaults.standard.integer(forKey: "addedCard") == 1 {
            logoutButt.setTitle("Back".localized(), for: .normal)
            backButton.isHidden = false
        }
        self.view.viewWithTag(2)?.heroID = "topView"
        loadCountryData()
        imagePicker.delegate = self
        pickerInitialize()
        
        cityTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .never, textColor: .gray, backgroundColor: .clear, size: nil)
        countryTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .never, textColor: .gray, backgroundColor: .clear, size: nil)
        genderTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .never, textColor: .gray, backgroundColor: .clear, size: nil)
        jobDataTxtFiled.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .never, textColor: .gray, backgroundColor: .clear, size: nil)
        jobTxtField.setRightViewFAIcon(icon: .FAAngleDown, rightViewMode: .never, textColor: .gray, backgroundColor: .clear, size: nil)
        
        cityTxtField.isEnabled = false
        cityTxtField.addPadding(UITextField.PaddingSide.left(20))
        countryTxtField.addPadding(UITextField.PaddingSide.left(20))
        genderTxtField.addPadding(UITextField.PaddingSide.left(20))
        WebsiteTxtField.addPadding(UITextField.PaddingSide.left(20))
        twitterTxtField.addPadding(UITextField.PaddingSide.left(20))
        snapchatTxtField.addPadding(UITextField.PaddingSide.left(20))
        instgramTxtField.addPadding(UITextField.PaddingSide.left(20))
        jobTxtField.addPadding(UITextField.PaddingSide.left(20))
        companyTxtField.addPadding(UITextField.PaddingSide.left(20))
        jobDataTxtFiled.addPadding(UITextField.PaddingSide.left(20))
        facebookTxtField.addPadding(UITextField.PaddingSide.left(20))
        emailField.addPadding(UITextField.PaddingSide.left(20))
        linkedInField.addPadding(UITextField.PaddingSide.left(20))
        DetailsAboutYouTextView.placeholder = "About".localized()
        if L102Language.isRTL {
            DetailsAboutYouTextView.textAlignment = .right
        }else{
            DetailsAboutYouTextView.textAlignment = .left
        }
        DetailsAboutYouTextView.placeholderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        showCountryPicker()
        showCityPicker()
        showGenderPicker()
        showJobPicker()
        showJobDataPicker()
        DetailsAboutYouTextView.delegate = self
        AlertControllerToGetImage()
    }
    func loadCountryData() {
        HUD.show(.progress)
        userData.Instance.fetchUser()
        CountryMain.Instance.getCountriesServer(enterDoStuff: { () in
            self.words = CountryMain.Instance.getCountryNameArr()
            if self.isEditing {
                for country in CountryMain.Instance.countryData.data {
                    if country.id == self.card.data?.city?.country?.id {
                        self.selectedCountry = country
                        break
                    }}
                self.countryTxtField.text = self.selectedCountry.name ?? ""
                self.words1 = self.selectedCountry.getCitiesName()
                self.picker1.reloadAllComponents()
                
            }
            self.picker.reloadAllComponents()
            HUD.hide()
        })
        WorkSectorAndPosition.Instance.getWorkSectors(enterDoStuff: { () in
            self.words3 = WorkSectorAndPosition.Instance.getWorkSectorNames()
            self.picker3.reloadAllComponents()
            HUD.hide()
        })
        WorkSectorAndPosition.Instance.getPositions(enterDoStuff: { () in
            self.words4 = WorkSectorAndPosition.Instance.getPositionsNames()
            self.picker4.reloadAllComponents()
            HUD.hide()
        })
        
    }
    func pickerInitialize(){
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker1.delegate = self
        picker1.dataSource = self
        picker1.backgroundColor = .white
        picker1.showsSelectionIndicator = true
        picker2.delegate = self
        picker2.dataSource = self
        picker2.backgroundColor = .white
        picker2.showsSelectionIndicator = true
        picker3.delegate = self
        picker3.dataSource = self
        picker3.backgroundColor = .white
        picker3.showsSelectionIndicator = true
        picker4.delegate = self
        picker4.dataSource = self
        picker4.backgroundColor = .white
        picker4.showsSelectionIndicator = true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let newTextCount = Int(newText.count)
        if newTextCount <= 200  {
            self.counterLabel.text = "200/\(newText.count)"
        }
        return newText.count < 200
        
    }
    
    func showCountryPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        countryTxtField.inputView = picker
        countryTxtField.inputAccessoryView = toolbar
        
    }
    func AlertControllerToGetImage() {
        alertController = UIAlertController(title: "Get Image".localized(), message: "from Gallery or Camera".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera".localized(), style: UIAlertActionStyle.default) { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let GalleryAction = UIAlertAction(title: "Gallery".localized(), style: UIAlertActionStyle.default) { (action) in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cameraAction)
        alertController.addAction(GalleryAction)
        alertController.addAction(cancelAction)

        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            profilePicture.image = image
            imageFlag = 1
        }
        self.dismiss(animated: true, completion: nil)
    }
    func showCityPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done1Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cityTxtField.inputView = picker1
        cityTxtField.inputAccessoryView = toolbar
        
        
    }
    
    func showGenderPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done2Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        genderTxtField.inputView = picker2
        genderTxtField.inputAccessoryView = toolbar
        
    }
    
    func showJobPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done3Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        jobTxtField.inputView = picker3
        jobTxtField.inputAccessoryView = toolbar
        
    }
    
    func showJobDataPicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(done4Picker));
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        jobDataTxtFiled.inputView = picker4
        jobDataTxtFiled.inputAccessoryView = toolbar
        
    }
    
    @objc func donePicker(){
        if countryTxtField.text == "" {
            if words.count > 0 {
                countryTxtField.text = words[0]
                selectedCountry = CountryMain.Instance.countryData.data[0]
                words1 = selectedCountry.getCitiesName()
                self.picker1.reloadAllComponents()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        cityTxtField.isEnabled = true
        
    }
    @objc func done1Picker(){
        if cityTxtField.text == "" {
            if words1.count > 0 {
                cityTxtField.text = words1[0]
                cityId =  selectedCountry.cities?[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func done2Picker(){
        
        if genderTxtField.text == "" {
            if words2.count > 0 {
                genderTxtField.text = words2[0].localized()
                selectedGender = words2[0]
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func done3Picker(){
        if jobTxtField.text == "" {
            if words3.count > 0 {
                jobTxtField.text = words3[0]
                workSectorId = WorkSectorAndPosition.Instance.workSector.data[0].id ?? 0
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func done4Picker(){
        if jobDataTxtFiled.text == "" {
            if words4.count > 0 {
                jobDataTxtFiled.text = words4[0]
                positionId =  WorkSectorAndPosition.Instance.position.data[0].id ?? 0
                
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == picker {
            return words.count
        }
        if pickerView == picker1 {
            return words1.count
        }
        if pickerView == picker2 {
            return words2.count
        }
        if pickerView == picker3{
            return words3.count
        }
        if pickerView == picker4 {
            return words4.count
        }
        
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == picker {
            return "\(words[row])"
            
        }
        if pickerView == picker1 {
            return "\(words1[row])"
        }
        if pickerView == picker2 {
            return "\(words2[row].localized())"
            
        }
        if pickerView == picker3 {
            return "\(words3[row])"
        }
        if pickerView == picker4 {
            return "\(words4[row])"
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == picker {
            if words.count > 0 {
                selectedCountry = CountryMain.Instance.countryData.data[row]
                words1 = selectedCountry.getCitiesName()
                countryTxtField.text =  words[row]
                self.picker1.reloadAllComponents()
                cityId =  -1
                cityTxtField.text = ""
            }  else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == picker1 {
            if words1.count > 0 {
                selected1 = words1[row]
                cityId =  selectedCountry.cities?[row].id ?? 0
                cityTxtField.text = selected1
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == picker2 {
            genderTxtField.text = words2[row].localized()
            selectedGender = words2[row]
        }
        if pickerView == picker3 {
            if words3.count > 0 {
                selected3 = words3[row]
                workSectorId = WorkSectorAndPosition.Instance.workSector.data[row].id ?? 0
                jobTxtField.text = selected3
                
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        if pickerView == picker4 {
            if words4.count > 0 {
                selected4 = words4[row]
                
                positionId =  WorkSectorAndPosition.Instance.position.data[row].id ?? 0
                jobDataTxtFiled.text = selected4
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    @IBAction func setPhoto(_ sender: Any) {
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func setPhotoCamera(_ sender: Any) {
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func addCard(_ sender: Any) {
        if imageFlag == -1 {
            makeErrorAlert(subTitle:  "Please Enter your profile Picture please".localized())
            return
        }
        if emailField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your Email".localized())
            return
        }
        if nameTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your Name".localized())
            return
        }
        if countryTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your country".localized())
            return
        }
        if cityTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your city".localized())
            return
        }
        if genderTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your gender".localized())
            return
        }
        if jobTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your job".localized())
            return
        }
        if jobDataTxtFiled.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your job Data".localized())
            return
        }
        if phoneTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your phone".localized())
            return
        }
        if mobileTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your mobile".localized())
            return
        }
        if addressTxtField.text == "" {
            makeErrorAlert(subTitle:  "Please Enter your address".localized())
            return
        }
        if companyTxtField.text == ""   {
            makeErrorAlert(subTitle:  "Please Enter your company".localized())
            return
        }
            let img = (profilePicture.image ?? UIImage()).wxCompress()
            var par:[String : Any] =
                ["name": nameTxtField.text ?? "",
                 "brief": DetailsAboutYouTextView.text ?? "" ,
                 "avatar": UIImageJPEGRepresentation(img, 0)?.base64EncodedString() ?? "" ,
                 "gender": selectedGender ,
                 "city_id": cityId ,
                 "position_id": positionId ,
                 "work_sector_id": workSectorId ,
                 "company": companyTxtField.text ?? "" ,
                 "phone": phoneTxtField.text ?? "" ,
                 "mobile": mobileTxtField.text  ?? "" ,
                 "address": addressTxtField.text ?? "" ,
                 "is_default": true
            ]
            
            if emailField.text != "" {
                if let temp1 = emailField.text  {
                    par.updateValue(temp1, forKey: "email")
                }
            }
            if WebsiteTxtField.text != "" {
                if let temp1 = WebsiteTxtField.text  {
                    par.updateValue("http://www.\(temp1)", forKey: "social_media[website]")
                }
            }
            if facebookTxtField.text != "" {
                if let temp2 = facebookTxtField.text  {
                    par.updateValue("https://www.facebook.com/\(temp2)", forKey: "social_media[facebook]")
                }
            }
            if instgramTxtField.text != "" {
                if let temp3 = instgramTxtField.text {
                    par.updateValue("https://www.instagram.com/\(temp3)", forKey: "social_media[instagram]")
                }
            }
            if snapchatTxtField.text != "" {
                if let temp4 = snapchatTxtField.text  {
                    par.updateValue("https://www.snapchat.com/add/\(temp4)", forKey: "social_media[snapchat]")
                }
            }
            if twitterTxtField.text != "" {
                if let temp5 = twitterTxtField.text  {
                    par.updateValue("https://twitter.com/\(temp5)" , forKey: "social_media[twitter]")
                }
            }
            if linkedInField.text != "" {
                if let temp5 = linkedInField.text  {
                    par.updateValue("https://www.linkedin.com/in/\(temp5)" , forKey: "social_media[linkedin]")
                }
            }
            print(par)
            if isEditing{
                updateCard(par:par)
                return
            }
            let vc = AppStoryboard.Main.viewController(viewControllerClass: ChooseDesignViewController.self)
            vc.par = par as [String : AnyObject]
            vc.countryNam = countryTxtField.text ?? ""
            vc.cityName = cityTxtField.text ?? ""
            vc.jobPosition = jobTxtField.text ?? ""
            vc.cardImg = profilePicture.image ?? UIImage()
            self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateCard(par:[String:Any]){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.updateCard(id: card.data?.id ?? 0), method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(value)
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        makeErrorAlert(subTitle: err.parseError())
                        
                        
                    }catch{
                        print(error)
                    }
                }else{
                    do {
                        let _ = try JSONDecoder().decode(Card.self, from: response.data!)
                        print("success")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Reload"), object: nil, userInfo: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadInner"), object: nil, userInfo: nil)
                        self.dismiss(animated: true, completion: nil)
                    }catch{
                        makeErrorAlert(subTitle: "Something went wrong, please try again later")
                    }
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
        
    }
    
    func parseCard(){
        nameTxtField.text = card.data?.name ?? ""
        DetailsAboutYouTextView.text = card.data?.brief ?? ""
        counterLabel.text = "200/\(DetailsAboutYouTextView.text.count)"
        cityId = card.data?.city?.id ?? 0
        cityTxtField.text = card.data?.city?.name ?? ""
        genderTxtField.text = (card.data?.gender ?? "").localized()
        selectedGender = card.data?.gender ?? ""
        jobDataTxtFiled.text = card.data?.position?.name ?? ""
        positionId = card.data?.position?.id ?? 0
        jobTxtField.text = card.data?.work_sector?.name ?? ""
        workSectorId = card.data?.work_sector?.id ?? 0
        companyTxtField.text = card.data?.company ?? ""
        mobileTxtField.text = card.data?.mobile ?? ""
        phoneTxtField.text = card.data?.phone ?? ""
        addressTxtField.text = card.data?.address ?? ""
        emailField.text = card.data?.email ?? ""
        WebsiteTxtField.text = (card.data?.social_media?.website ?? "").replacingOccurrences(of: "http://www.", with: "").replacingOccurrences(of: "https://", with: "").replacingOccurrences(of: "www.", with: "")
        print("innn")
        print(card.data?.social_media?.website ?? "".replacingOccurrences(of: "http://www.", with: ""))
        facebookTxtField.text = (card.data?.social_media?.facebook ?? "").replacingOccurrences(of: "https://www.facebook.com/", with: "")
        twitterTxtField.text = (card.data?.social_media?.twitter ?? "").replacingOccurrences(of: "https://twitter.com/", with: "")
        snapchatTxtField.text = (card.data?.social_media?.snapchat ?? "").replacingOccurrences(of: "https://www.snapchat.com/add/", with: "")
        instgramTxtField.text = (card.data?.social_media?.instagram ?? "").replacingOccurrences(of: "https://www.instagram.com/", with: "")
        linkedInField.text = (card.data?.social_media?.linkedin ?? "").replacingOccurrences(of: "https://www.linkedin.com/in/", with: "")
        profilePicture.sd_setImage(with: URL(string: card.data?.avatar ?? "")) { (image, error, cache, urls) in
            if (error != nil) {
                self.profilePicture.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
            } else {
                self.profilePicture.backgroundColor = .white
                self.profilePicture.image = image
            }}
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "submitCard" {
            let destination = segue.destination as? ChooseDesignViewController
            destination?.cardId = yourCardId
        }
    }
}

