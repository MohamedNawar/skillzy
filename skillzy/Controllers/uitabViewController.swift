//
//  uitabViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/16/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class uitabViewController: UITabBarController {

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "My Cards".localized() {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Reload"), object: nil, userInfo: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationController?.toolbar  //DroidArabicKufi
        
    NotificationCenter.default.addObserver(self, selector: #selector(redirectUser), name: NSNotification.Name(rawValue: "Redirect"), object: nil)
        let temp = self.tabBar as! MainTabBar
        temp.BtnPressed = { () in
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "QRScannerController") as! QRScannerController
             let navQr = UINavigationController.init(rootViewController: newViewController)
            self.present(navQr, animated: true, completion: nil)
            
        }
        setupTabBar()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        redirectUser()

    }
    @objc func redirectUser(){
        if !userData.Instance.isLogged(){
            let loginVC =  storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            let navLogin = UINavigationController.init(rootViewController: loginVC ?? UIViewController())
            navLogin.modalPresentationStyle = .overFullScreen
            self.present(navLogin, animated: true, completion: nil)
        }


        else if UserDefaults.standard.integer(forKey: "addedCard") == 0{
            if userData.Instance.data?.cards_count == 0 || userData.Instance.data?.cards_count == nil {
                //uncomment on final release
                let loginVC =  storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                let navLogin = UINavigationController.init(rootViewController: loginVC)
                navLogin.modalPresentationStyle = .overFullScreen
                loginVC.goToCardView = true
                self.present(navLogin, animated: false, completion: nil)
            }
            
        }
    }
    func setupTabBar(){
        let myCardsNavVC =  storyboard?.instantiateViewController(withIdentifier: "TabeViewControllerNavVC")
        myCardsNavVC?.tabBarItem = UITabBarItem(title: "My Cards".localized(), image: UIImage(named: "my cards"), selectedImage: UIImage(named: "my cards"))
        
        
        
        let searchVC = AppStoryboard.Main.viewController(viewControllerClass: SearchViewController.self)
        searchVC.tabBarItem = UITabBarItem(title: "Search".localized(), image: UIImage(named: "search"), selectedImage: UIImage(named: "search"))
        let navSearch = UINavigationController.init(rootViewController: searchVC)
        
        
        let mapVC = AppStoryboard.SecondSB.viewController(viewControllerClass: MainMapVC.self)
        mapVC.tabBarItem = UITabBarItem(title: "Nearby".localized(), image: UIImage(named: "Map"), selectedImage: UIImage(named: "Map"))
        
        
        let navMap = UINavigationController.init(rootViewController: mapVC)
        
        
        let vc = UIViewController()
        
        vc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: ""), selectedImage: UIImage(named: ""))
        
        let navSettings = storyboard?.instantiateViewController(withIdentifier: "SettingViewControllerNavVC")
        navSettings?.tabBarItem = UITabBarItem(title: "Settings".localized(), image: UIImage(named: "baseline-settings-20px (1)"), selectedImage: UIImage(named: "baseline-settings-20px (1)"))
        
        
        
        
        self.viewControllers = [myCardsNavVC ?? UIViewController(), navSearch, vc ,navMap, navSettings ?? UIViewController()]
    }
}
