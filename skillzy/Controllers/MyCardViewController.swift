//
//  FirstTableViewController.swift
//  Rento
//
//  Created by MACBOOK on 7/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class MyCardViewController: UIViewController , UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCardCell", for: indexPath)
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
      
    }
}
