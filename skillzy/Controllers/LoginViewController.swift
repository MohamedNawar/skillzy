//
//  LoginViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/11/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
class LoginViewController: UIViewController , UITextFieldDelegate,LanguageDelegate {
    var goToCardView = false
    @IBAction func registerPreessed(_ sender: Any) {
        self.navigationController?.hero.isEnabled = true
        view.endEditing(true)
        self.view.viewWithTag(1)?.heroID = "loginButt"
        let vc = storyboard?.instantiateViewController(withIdentifier: "EnterMailAndPasswordViewController")
        vc?.navigationController?.hero.isEnabled = true
        self.show(vc!, sender: self)
        
    }
    
    @IBAction func forgotPass(_ sender: Any) {
        view.endEditing(true)
        self.navigationController?.hero.isEnabled = true
        self.view.viewWithTag(1)?.heroID = "loginButt"
        let vc = storyboard?.instantiateViewController(withIdentifier: "frstFPVC")
            //AppStoryboard.Main.viewController(viewControllerClass: frstFPVC.self)
        vc?.navigationController?.hero.isEnabled = true
        self.show(vc!, sender: self)
    }
    @IBOutlet weak var passwordlTextField: DesignableTextField!
    @IBOutlet weak var emailTextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.integer(forKey: "selectedLang") == 0 {
            let langVC =  AppStoryboard.Main.viewController(viewControllerClass: changeLanguageVC.self)
            langVC.delegate = self
            langVC.modalPresentationStyle = .overFullScreen
            self.present(langVC, animated: true, completion: nil)
        }
        self.navigationController?.navigationBar.isHidden = true
        passwordlTextField.delegate = self
        emailTextField.delegate = self
        if goToCardView{
            let vc = AppStoryboard.Main.viewController(viewControllerClass: EnterDataViewController.self)
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        }
        //        if goToCardView {
        //            let vc = storyboard?.instantiateViewController(withIdentifier: "EnterDataViewController")
        //            self.navigationController?.pushViewController(vc!, animated: false)
        //        }
    }
    @IBAction func LoginBtn(_ sender: Any) {
        if passwordlTextField.text == "" || emailTextField.text == "" {
            HUD.flash(.label("حقل فارغ"), delay: 1.0)
        }else{
            login()
        }
    }
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func login(){
        let header = APIs.Instance.getHeader()
        var par = ["email": emailTextField.text!, "password": passwordlTextField.text!]
        if (UserDefaults.standard.string(forKey: "onesignalid") ?? "") != ""{
            par["onesignal-player-id"] = UserDefaults.standard.string(forKey: "onesignalid") ?? ""
        }
        print(par)
        HUD.dimsBackground = false
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.login(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                print(value)
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        print(err.parseError())
                        makeErrorAlert(subTitle: err.parseError())
                        
                    }catch{
                        print(error)
                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    
                    do {
                        HUD.flash(.success, delay: 1.0)
                        UserDefaults.standard.set(response.data!, forKey: "user")
                        userData.Instance.fetchUser()
                        UIApplication.shared.registerForRemoteNotifications()
                        if userData.Instance.data?.cards_count ?? 0 > 0 {
                            UserDefaults.standard.set(1, forKey: "addedCard")
                        }
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Reload"), object: nil, userInfo: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Redirect"), object: nil, userInfo: nil)
                        if userData.Instance.data?.cards_count == 0 {
                            let vc = AppStoryboard.Main.viewController(viewControllerClass: EnterDataViewController.self)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                        self.dismiss(animated: true, completion: nil)}
                        
                    }catch{
                        HUD.flash(.labeledError(title: "", subtitle: "حدث خطأ برجاء اعادة المحاولة"), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        self.navigationController?.navigationBar.isHidden = false
    }
    func didSelectLang(lang: String) {
        UserDefaults.standard.set(1, forKey: "selectedLang")
        if lang == L102Language.currentAppleLanguage() {
            return
        }
        L102Language.setAppleLAnguageTo(lang: lang)
        if lang == "ar"{
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        };        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "uitabViewController")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0, options: .transitionFlipFromLeft, animations: { () -> Void in
        }) { (finished) -> Void in
        }
    }
    
}


