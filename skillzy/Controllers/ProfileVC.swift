//
//  ProfileVC.swift
//  skillzy
//
//  Created by Moaz Ezz on 9/16/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
import FCAlertView

class ProfileVC: UIViewController {
    let hud = JGProgressHUD(style: .light)
    
    @IBOutlet var currentEmail: UILabel!
    @IBOutlet weak var email: DesignableTextField!
    
    @IBOutlet weak var oldPass: DesignableTextField!
    
    @IBOutlet weak var newPass: DesignableTextField!
    
    @IBOutlet weak var newPassConfiermed: DesignableTextField!
    
    
    
    
    @IBAction func savePressed(_ sender: Any) {
        postPassworrdUpdate()
    }
    
    @IBAction func updateMail(_ sender: Any) {
        postEmailUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    
    private func setupView(){
        
    }
    private func postEmailUpdate(){
        if email.text == "" {
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.show(in: self.view)
            self.hud.textLabel.text = "Empty Field"
            self.hud.dismiss(afterDelay: 1.0)
            return}
        let par = ["email":email.text ?? ""]
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.updateProfile() , method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                print(value)
                self.hud.dismiss()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        //Fail
                        let err = try
                            JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "ic_koloda"))
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }else{
                    //Success
                    do {
                        let user = try JSONDecoder().decode(userData.self, from: response.data!)
                        userData.Instance.data?.email = user.data?.email
                        userData.Instance.data?.is_listed = user.data?.is_listed
                        print(userData.Instance.data?.is_listed)
                        self.currentEmail.text = "Current Email:".localized() + (self.email.text ?? "")
                        UserDefaults.standard.set("updatedEmail", forKey: (self.email.text ?? ""))
                        self.email.text = ""
                        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        self.hud.show(in: self.view)
                        self.hud.dismiss(afterDelay: 1.0)
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
    }
    
    private func postPassworrdUpdate(){
        guard newPass.text == newPassConfiermed.text else {
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.show(in: self.view)
            self.hud.textLabel.text = "Passwords Don't Match"
            self.hud.dismiss(afterDelay: 1.0)
            return
        }
        if newPass.text == "" || oldPass.text == "" || newPassConfiermed.text == "" {
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.show(in: self.view)
            self.hud.textLabel.text = "Empty Field"
            self.hud.dismiss(afterDelay: 1.0)
            return
        }
        
        
        hud.show(in: self.view)
        //        HUD.show(.progress, onView: self.view)
        
        
        let header = APIs.Instance.getHeader()
        var par = [String:Any]()
        if newPass.text != "" && oldPass.text != ""{
            par["password"] = newPass.text
            par["password_confirmation"] = newPassConfiermed.text
            par["old_password"] = oldPass.text
        }
        print(APIs.Instance.updateProfile())
        Alamofire.request(APIs.Instance.updateProfile() , method: .patch, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(value)
                self.hud.dismiss()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        //Fail
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "ic_koloda"))
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }else{
                    //Success
                    do {
   
                        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        self.hud.show(in: self.view)
                        self.hud.dismiss(afterDelay: 1.0)
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.string(forKey: "updatedEmail") == "" || UserDefaults.standard.string(forKey: "updatedEmail") ==  nil{
            currentEmail.text = "Current Email:".localized() + (userData.Instance.data?.email ?? "")
        }else{
            currentEmail.text = "Current Email:".localized() + (UserDefaults.standard.string(forKey: "updatedEmail") ?? "")
        }
    }
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
}
