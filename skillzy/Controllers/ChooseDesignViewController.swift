//
//  ChooseDesignViewController.swift
//  skillzy
//
//  Created by Mohamed Nawar on 8/29/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import SDWebImage

class ChooseDesignViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    var cardId = 34
    var moreAndLess = 3
    var flag = true
    var myCard = Card()
    var par = [String:AnyObject]()
    var jobPosition = ""
    var cityName = ""
    var workSector = ""
    var countryNam = ""
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        save()
    }
    
    var briefValue = ""
    var cardImg = UIImage()
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var moreLessBtn: UIButton!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var qrImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profilePicture.image = cardImg
        self.name.text = "\(par["name"]  ?? "----" as AnyObject)"
        self.position.text = jobPosition
        self.company.text = "\(par["company"]  ?? "----" as AnyObject)"
        //getCardsData()
        
        tableView1.separatorStyle = .none
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        if section == 1 {
            return moreAndLess
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "aboutCell", for: indexPath)
            cell.selectionStyle = .none
            let lbl = cell.viewWithTag(11) as! UILabel
            lbl.text =  "\(par["brief"]  ?? "----" as AnyObject)"
            return cell
        }
        if indexPath.section == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "informationCell", for: indexPath)
            cell.selectionStyle = .none
            if indexPath.row == 0 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"businessman") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "JOB POSITION".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  jobPosition
            }
            if indexPath.row == 1 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"company") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "COMPANY".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["company"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 1 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"company") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "COMPANY".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["company"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 2 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"phone-call (3)") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "TELEPHONE NUMBER".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["phone"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 3 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"smartphone-call") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "MOBILE NUMBER".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text = "\(par["mobile"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 4 {
                let img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"placeholder") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "ADDRESS".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["address"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 5 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"grid-world (1)") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "WEBSITE".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text = "\(par["social_media[website]"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 6 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"email") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "E-MAIL".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["email"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 7 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"facebook-1") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "FACEBOOK ACCOUNT".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["social_media[facebook]"]  ?? "----" as AnyObject)"
                
            }
            if indexPath.row == 8 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"twitter (1)") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "TWITTER ACCOUNT".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["social_media[twitter]"]  ?? "----" as AnyObject)"
            }
            if indexPath.row == 9 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"snapchat (1)") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "SNAPCHAT ACCOUNT".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["social_media[snapchat]"]  ?? "----" as AnyObject)"
                
            }
            if indexPath.row == 10 {
                var img = cell.viewWithTag(14) as! UIImageView
                img.image =  UIImage(named:"instagram (3)") ?? UIImage()
                let lbl = cell.viewWithTag(12) as! UILabel
                lbl.text =  "INSTAGRAM ACCOUNT".localized()
                let lbl1 = cell.viewWithTag(13) as! UILabel
                lbl1.text =  "\(par["social_media[instagram]"]  ?? "----" as AnyObject)"
                
            }
            return cell
        }
        return cell
    }
    @IBAction func lessMoreBtn(_ sender: Any) {
        
        if flag  {
            moreAndLess = 11
            moreLessBtn.setTitle("Less".localized(), for: .normal)
        }else {
            moreAndLess = 3
             moreLessBtn.setTitle("More".localized(), for: .normal)
        }
         tableView1.reloadData()
        flag = !flag
    }
    func save(){
        let header = APIs.Instance.getHeader()
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.storeCard(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(value)
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        makeErrorAlert(subTitle: err.parseError())
                        
                        
                    }catch{
                        print(error)
                    }
                }else{
                    do {
                        let _ = try JSONDecoder().decode(Card.self, from: response.data!)
                        print("success")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Reload"), object: nil, userInfo: nil)
                        UserDefaults.standard.set(1, forKey: "addedCard")
                        self.dismiss(animated: true, completion: nil)
                    }catch{
                        makeErrorAlert(subTitle: "Something went wrong, please try again later")
                    }
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    
    }

}
