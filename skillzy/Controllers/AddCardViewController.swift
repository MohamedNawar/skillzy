//
//  AddCardViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/9/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit

class AddCardViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
  

    @IBOutlet weak var addCardTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addCardTableView.delegate = self
        addCardTableView.dataSource = self
        addCardTableView.separatorStyle = .none
}
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        if indexPath.row == 0 {
        cell = addCardTableView.dequeueReusableCell(withIdentifier: "AddCardDetailsCell", for: indexPath)
        return cell
        }
        if indexPath.row == 1 {
             cell = addCardTableView.dequeueReusableCell(withIdentifier: "AddCardCell", for: indexPath)
            return cell

        }
        return cell
    }
    
}
