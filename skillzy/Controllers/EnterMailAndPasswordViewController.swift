//
//  EnterMailAndPasswordViewController.swift
//  skillzy
//
//  Created by MACBOOK on 8/11/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import Hero
import FCAlertView
class EnterMailAndPasswordViewController: UIViewController , UITextFieldDelegate {
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var topView: UIView!
    @IBOutlet weak var passwordlTextField: DesignableTextField!
    @IBOutlet weak var emailTextField: DesignableTextField!
    override func viewDidLoad() {
        self.view.viewWithTag(1)?.hero.id = "loginButt"
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        
        passwordlTextField.delegate = self
        emailTextField.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.borderColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        textField.borderWidth = 1
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.borderColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        }
    }
    @IBAction func Next(_ sender: Any) {
        if passwordlTextField.text == "" || emailTextField.text == "" {
            makeErrorAlert(subTitle: "Please fill in the missing fields".localized())

        }else{
            
       
            PostProfileInfo()
        }
    }
    
    
    func PostProfileInfo(){
        
        if self.emailTextField.text == "" || self.passwordlTextField.text == "" {
            HUD.flash(.labeledError(title: "حقل فارغ", subtitle: ""), delay: 1.0)
            return
        }
        let header = APIs.Instance.getHeader()
        var par = ["email": emailTextField.text!, "password": passwordlTextField.text! ]
        if (UserDefaults.standard.string(forKey: "onesignalid") ?? "") != ""{
            par["onesignal-player-id"] = UserDefaults.standard.string(forKey: "onesignalid") ?? ""
        }
        HUD.show(.progress)
        print(APIs.Instance.registeration())
        Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(value)
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        makeErrorAlert(subTitle: err.parseError())
                    }catch{
                        HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                    }
                }else{
                    do {
                        
                        UserDefaults.standard.set(response.data!, forKey: "user")
                        userData.Instance.fetchUser()
                        UIApplication.shared.registerForRemoteNotifications()
                        //EnterDataViewController
                        self.view.viewWithTag(2)?.heroID = "topView"
                        self.navigationController?.hero.isEnabled = true
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterDataViewController")
                        vc?.navigationController?.hero.isEnabled = true
                        self.show(vc!, sender: self)
                        
                    }catch{
                    
                    }
                    HUD.flash(.success, delay: 1.0)
                }
            case .failure(_):
                HUD.hide()
                HUD.flash(.labeledError(title: "", subtitle:"حدث خطأ برجاء إعادة المحاولة لاحقاً"), delay: 1.0)
                break
            }
        }
    
    }
    
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = true
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
}
