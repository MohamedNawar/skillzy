//
//  RandomCardsVC.swift
//  Koloda
//
//  Created by Eugene Andreyev on 4/23/15.
//  Copyright (c) 2015 Eugene Andreyev. All rights reserved.
//

import UIKit
import Koloda
import Alamofire
import JGProgressHUD
import FCAlertView
import PKHUD



class RandomCardsVC: UIViewController {
    let hud = JGProgressHUD(style: .light)
    var cards = [CardData]()
    var flag = false
    @IBOutlet weak var kolodaView: KolodaView!
    
    @IBOutlet var bottomStack: UIStackView!
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Feeling Lucky".localized()
       
        if L102Language.isRTL {
            bottomStack.addArrangedSubview(self.bottomStack.subviews[0])
        }
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        kolodaView.dataSource = self
        kolodaView.delegate = self
        
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        getRandomeCardWithUi()
        getRandomeCardWithUi()
        getRandomeCardWithUi()
        getRandomeCardWithUi()
        getRandomeCardWithUi()
    }
    
    
    // MARK: IBActions
    @IBAction func reject(_ sender: Any) {
        if !cards.isEmpty{
            kolodaView?.swipe(.left)
        }
    }
    @IBAction func accept(_ sender: Any) {
        if !cards.isEmpty{
            kolodaView?.swipe(.right)
        }
    }
    
    
    
    private func getRandomeCardWithUi(){
        hud.show(in: self.view)
        //        HUD.show(.progress, onView: self.view)
        let header = APIs.Instance.getHeader()
       
        
        print(APIs.Instance.getRandomCard())
        Alamofire.request(APIs.Instance.getRandomCard(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(value)
                self.hud.dismiss()
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        //Fail
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "ic_koloda"))
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }else{
                    //Success
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                print(temp2)
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                print("---------")
                                print(temp3)
                                let t = try JSONDecoder().decode(CardData.self, from: temp3)
                                self.cards.append(t)
                                self.kolodaView.reloadData()
                                //                                self.pares(temp: temp3)
                            }catch{
                                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                                self.hud.dismiss(afterDelay: 1.0)
                                
                            }
                        }
                    }
                    
                }
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
    private func getRandomeCardWithoutUi(){
        
        //        HUD.show(.progress, onView: self.view)
        let header = APIs.Instance.getHeader()
        
        
        print(APIs.Instance.getRandomCard())
        Alamofire.request(APIs.Instance.getRandomCard(), method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(value)
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        //Fail
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "ic_koloda"))
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }else{
                    //Success
                    if let temp = value as? Dictionary<String,AnyObject>{
                        if let temp2 = temp["data"]{
                            
                            
                            do {
                                print(temp2)
                                let temp3 = try JSONSerialization.data(withJSONObject: temp2, options: .prettyPrinted)
                                print("---------")
                                print(temp3)
                                let t = try JSONDecoder().decode(CardData.self, from: temp3)
                                self.cards.append(t)
                                self.kolodaView.reloadData()
                                //                                self.pares(temp: temp3)
                            }catch{
                                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                                self.hud.dismiss(afterDelay: 1.0)
                                
                            }
                        }
                    }
                    
                }
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
    private func postRandomeCardWithoutUi(id:Int,type:Bool){
        
        //        HUD.show(.progress, onView: self.view)
        let header = APIs.Instance.getHeader()
        
        var method = HTTPMethod.post
        
        if type {
            method = HTTPMethod.post
        }else{
            method = HTTPMethod.delete
        }
        
        
        print(APIs.Instance.addCardToContacts(id: id))
        Alamofire.request(APIs.Instance.addCardToContacts(id: id), method: method, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(value)
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        //Fail
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "ic_koloda"))
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }else{
                    //Success
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Reload"), object: nil, userInfo: nil)
                    print("Success")
                    
                }
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
}

// MARK: KolodaViewDelegate

extension RandomCardsVC: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
//        HUD.show(.label("No more Cards"), onView: koloda)
        getRandomeCardWithUi()
        getRandomeCardWithUi()
        getRandomeCardWithUi()
        getRandomeCardWithUi()
        getRandomeCardWithUi()
    }
    
//    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
//        UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
//    }
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: CardInnerVC.self)
        
        vc.card = (cards[index])// ?? CardData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        getRandomeCardWithoutUi()
        
        if direction == .right{
            postRandomeCardWithoutUi(id: cards[index].id ?? -1,type:true)
        }else if direction == .left{
            print("ToDo -> Check this link because it returns errors and check the logic also")
//            postRandomeCardWithoutUi(id: cards[index].id ?? -1,type:false)
        }
//        cards.remove(at: index)
    }
    
}

// MARK: KolodaViewDataSource

extension RandomCardsVC: KolodaViewDataSource {
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        return cards.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .default
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        let tempView = Bundle.main.loadNibNamed("swipableCard", owner: self, options: nil)?[0] as! swipableCard
        tempView.layer.cornerRadius = 10
        tempView.name.text = cards[index].name
        tempView.position.text = cards[index].position?.name
        tempView.location.text = "\(cards[index].city?.name ?? ""), \(cards[index].city?.country?.name ?? "")"
        let url = URL(string: cards[index].avatar ?? "")
        tempView.photo.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "baseline-perm_identity-24px"), options: .highPriority, completed: nil)
        return tempView
//        return UIImageView(image: dataSource[Int(index)])
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("OverlayView", owner: self, options: nil)?[0] as? OverlayView
    }
    
    
}

