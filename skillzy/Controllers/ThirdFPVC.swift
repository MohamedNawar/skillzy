//
//  ThirdFPVC.swift
//  Rafeeq
//
//  Created by mouhammed ali on 6/8/18.
//  Copyright © 2018 mouhammed ali. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects

class ThirdFPVC: UIViewController {
    
    var token = ""
    
    @IBAction func submitButt(_ sender: UIButton) {
        if textField.text != "" && confirmTeextField.text != "" {
            
            submitFunc()
        }else{
            showMissingFieldMsg()
        }
    }
    @IBOutlet var textField: UITextField!
    
    @IBOutlet var confirmTeextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        self.view.viewWithTag(2)?.hero.id = "forgetView"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    
    func submitFunc(){
        let par = ["token":token,
                   "password":textField.text!,
                   "password_confirmation":confirmTeextField.text!]
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.resetPassword()  , method: .post, parameters: par, encoding: URLEncoding.default, headers: APIs.Instance.getHeader()).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
                
                
            case .success(let value):
                // print(value)
                
                HUD.hide()
                print(value)
                if let dict = value as? [String:AnyObject] {
                    if let _ = dict["data"] as? [String:AnyObject]{
                        self.showSuccessAlert()
                    }
                    
                    
                    if let errors = dict["errors"] as? [String:AnyObject] {
                        var flag = 0
                        for value in errors.values {
                            if flag == 0 {
                                flag = 1
                                
                                if let strArr = value as? [String] {
                                    if strArr.count>0 {
                                        self.showError(msg:strArr[0], titleMsg: "error")}
                                }
                            }
                            break
                        }
                    }}
            case .failure(let error):
                print(error)
                
            }
        }
    }
    func showMissingFieldMsg(){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString("Error", comment: ""), withSubtitle: NSLocalizedString("Please fill in the new password", comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("OK", comment: ""), andButtons: nil)
    }
    func showError(msg:String, titleMsg:String){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString(titleMsg, comment: ""), withSubtitle: NSLocalizedString(msg, comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("Ok", comment: ""), andButtons: nil)
        
    }
    
    
    func showSuccessAlert(){
        let alert = FCAlertView()
        alert.makeAlertTypeSuccess()
        alert.firstButtonTitleColor = UIColor.white
        alert.doneBlock = {
            var viewControllers = self.navigationController?.viewControllers
            viewControllers?.removeLast(3) // views to pop
            self.navigationController?.setViewControllers(viewControllers!, animated: true)
        }
        
        
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString("Success", comment: ""), withSubtitle: "Password reset successfully".localized(), withCustomImage:nil, withDoneButtonTitle: NSLocalizedString("Done", comment: ""), andButtons: nil)
    }
    
    
    
}
