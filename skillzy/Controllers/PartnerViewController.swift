//
//  FirstTableViewController.swift
//  Rento
//
//  Created by MACBOOK on 7/21/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
class PartnerCell:UITableViewCell {
    
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var deleteButton: UIButton!
    var ButtonHandler:(()-> Void)!
    
    @IBAction func ButtonClicked(_ sender: UIButton) {
        self.ButtonHandler()
    }
    
}
class PartnerViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource {
    var searchPar = [String:AnyObject]()
    var emptyFlag = false
    var mode = 0        //0: my cards, 1: shared
    var card = CardArr()
    var isSearching = false
    var searchUrl = ""
    var reload = true
    @IBOutlet var tableHeightConstr: NSLayoutConstraint!
    
    @IBOutlet var topView: UIView!
    
    @IBAction func filterButtPressed(_ sender: UIButton) {
        self.navigationController?.hero.isEnabled = true
        sender.hero.id = "filterButt"
        let vc = AppStoryboard.Main.viewController(viewControllerClass: FilterForMapViewController.self)
        vc.navigationController?.hero.isEnabled = true
        vc.hero.isEnabled = true
        self.show(vc, sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if emptyFlag && card.data?.count == 0{
            return 1
        }
        return card.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if emptyFlag && card.data?.count == 0 {
            return
        }
        //let vc = AppStoryboard.Main.viewController(viewControllerClass: CardInnerVC.self)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CardInnerVC") as! CardInnerVC
        
        vc.card = (card.data?[indexPath.row])// ?? CardData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if emptyFlag && card.data?.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
            let descLbl = cell.viewWithTag(-1) as! UILabel
            descLbl.text = "No Shared Cards Were Added Yet!".localized()
            cell.selectionStyle = .none
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PartnerCell", for: indexPath) as! PartnerCell
        cell.selectionStyle = .none
        let item = card.data?[indexPath.row]
        cell.nameLbl.text = item?.name ?? ""
        cell.descLbl.text = item?.position?.name ?? ""
        cell.imgView.sd_setImage(with: URL(string: item?.avatar ?? "")) { (image, error, cache, urls) in
            if (error != nil) {
                cell.imgView.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
            } else {
                cell.imgView.backgroundColor = .white
                cell.imgView.image = image
            }
            
        }
        
        
        if L102Language.currentAppleLanguage() == "ar" {
            cell.nameLbl.textAlignment = .right
            cell.descLbl.textAlignment = .right
        }else{
            cell.nameLbl.textAlignment = .left
            cell.descLbl.textAlignment = .left
        }
        if mode == 1 {
            cell.deleteButton.isHidden = true
        }else{
            cell.deleteButton.isHidden = false
        }
        
        cell.ButtonHandler = {()-> Void in
            self.presentDeleteAlert(id:item?.id ?? 0)

        }
        
        return cell
    }
    
    func presentDeleteAlert(id:Int){
        let alertController = UIAlertController(title: "Delete".localized(), message: "Do you want to delete card?".localized(), preferredStyle: UIAlertControllerStyle.actionSheet)
        let deleteAction = UIAlertAction(title: "Delete".localized(), style: UIAlertActionStyle.destructive) { (action) in
            self.deleteCard(id: id)
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        present(alertController, animated: true, completion: nil)
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        getMyCads(hideHud:false)
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(getMyCads), name: NSNotification.Name(rawValue: "Reload"), object: nil)
        if mode == 1 {
            topView.isHidden = false
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        getMyCads(hideHud:true)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if mode == 0 {
            return true
        }
        return false
    }
    func deleteCard(id:Int){
        HUD.show(.progress)
        let header = APIs.Instance.getHeader()
        Alamofire.request(APIs.Instance.deleteCard(id: id), method: .delete, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success( _):
                self.getMyCads(hideHud: false)
            case .failure( _):
                break
            }
        }
    }
    @objc func getMyCads(hideHud:Bool){
        if !hideHud {
            HUD.show(.progress)
            
        }
        let header = APIs.Instance.getHeader()
        var url = APIs.Instance.getSharedCards()
        if mode == 0 {
            url = APIs.Instance.getMyCards()
            print(url)
        }
        if isSearching{
            url = searchUrl
        }
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            self.emptyFlag = true
            self.reload = false
            switch(response.result) {
            case .success(let value):
                print("mode:\(self.mode) vall:\(value)")
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    
                    do {
                        
                        self.card = try JSONDecoder().decode(CardArr.self, from: response.data!)
                        if (self.card.data?.count) ?? 0 > 0 {
                            self.emptyFlag = false
                            if self.mode == 1 {
                                self.topView.isHidden = false
                            }
                        }else{
                            if !self.isSearching {
                                self.topView.isHidden = true}
                        }
                        if self.mode == 0 && (self.card.data?.count) ?? 0 > 0 {
                            UserDefaults.standard.set(1, forKey: "addedCard")
                        }
                        self.tableView.reloadData()
                    }catch{
                        print("errrr:\(error)")
                    }
                }
                
            case .failure( _):
                break
            }
        }
    }
}
