//
//  TabeViewController.swift
//  Rento
//
//  Created by MACBOOK on 7/20/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Parchment
class TabeViewController: UIViewController {
    var secondViewController:PartnerViewController?
    @IBOutlet weak var MainView: UIView!
    
    @IBAction func addPressed(_ sender: Any) {
        let loginVC =  storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navLogin = UINavigationController.init(rootViewController: loginVC)
        navLogin.modalPresentationStyle = .overFullScreen
        loginVC.goToCardView = true
        self.present(navLogin, animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        self.navigationController?.hero.isEnabled = true
        self.view.viewWithTag(1)?.hero.id = "addButton"
        secondViewController = storyboard?.instantiateViewController(withIdentifier:"PartnerViewController") as! PartnerViewController
        secondViewController?.mode = 1
        let firstViewController = storyboard?.instantiateViewController(withIdentifier:"PartnerViewController") as! PartnerViewController
        
        firstViewController.title = "My Cards".localized()
        secondViewController?.title = "Shared".localized()
        var vcArr = [
            firstViewController,
            secondViewController ?? UIViewController(),
            ]
        if L102Language.isRTL {
            vcArr = [
                
                secondViewController ?? UIViewController(),firstViewController
                ]
        }
        let pagingViewController = FixedPagingViewController(viewControllers: vcArr)
    
            
        
        pagingViewController.textColor = UIColor.black
        pagingViewController.font =  UIFont(name: "NeoSansW23-Medium", size: 17) ?? UIFont.boldSystemFont(ofSize: 17)
        pagingViewController.selectedFont = UIFont(name: "NeoSansW23-Medium", size: 17) ?? UIFont.boldSystemFont(ofSize: 17)
        pagingViewController.selectedTextColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
        pagingViewController.indicatorColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        pagingViewController.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        pagingViewController.selectedBackgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)

        pagingViewController.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: view.frame.width/2, height: pagingViewController.menuItemSize.height + 10 )
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        pagingViewController.menuInteraction = .none
        
        pagingViewController.selectedScrollPosition = .preferCentered
        addChildViewController(pagingViewController)
        MainView.addSubview(pagingViewController.view)
        
        
        pagingViewController.didMove(toParentViewController: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        if L102Language.isRTL {
            pagingViewController.select(index: 1)
            
        }
        
        
        NSLayoutConstraint.activate([
            pagingViewController.view.leadingAnchor.constraint(equalTo: MainView.leadingAnchor),
            pagingViewController.view.trailingAnchor.constraint(equalTo: MainView.trailingAnchor),
            pagingViewController.view.bottomAnchor.constraint(equalTo: MainView.bottomAnchor),
            pagingViewController.view.topAnchor.constraint(equalTo: MainView.topAnchor )
            ])
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    func setupNavBar() {
        
        //        self.navigationController?.navigationBar.prefersLargeTitles = true
        //        self.navigationController?.navigationBar.isTranslucent = false
        //
        //        let searchController = UISearchController(searchResultsController: nil)
        //        self.navigationItem.searchController = searchController
        //
        //        let rightButton = UIButton()
        //        rightButton.setImage(#imageLiteral(resourceName: "add"), for: .normal)
        //        rightButton.setTitleColor(.purple, for: .normal)
        ////        rightButton.addTarget(self, action: #selector(rightButtonTapped(_:)), for: .touchUpInside)
        //        navigationController?.navigationBar.addSubview(rightButton)
        //        rightButton.tag = 1
        //        rightButton.frame = CGRect(x: self.view.frame.width, y: 0, width: 30, height: 30)
        //
        //        let targetView = self.navigationController?.navigationBar
        //
        //        let trailingContraint = NSLayoutConstraint(item: rightButton, attribute:
        //            .trailingMargin, relatedBy: .equal, toItem: targetView,
        //                             attribute: .trailingMargin, multiplier: 1.0, constant: -16)
        //        let bottomConstraint = NSLayoutConstraint(item: rightButton, attribute: .bottom, relatedBy: .equal,
        //                                                  toItem: targetView, attribute: .bottom, multiplier: 1.0, constant: -6)
        //        rightButton.translatesAutoresizingMaskIntoConstraints = false
        //        NSLayoutConstraint.activate([trailingContraint, bottomConstraint])
        
    }
    
    
}


