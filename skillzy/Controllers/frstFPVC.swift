//
//  firstFPVC.swift
//  skillzy
//
//  Created by mouhammed ali on 9/20/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import FCAlertView
import TextFieldEffects
class frstFPVC: UIViewController {

    @IBAction func submitButt(_ sender: Any) {
        if textField.text != "" {
            submitFunc()
        }else{
            showMissingFieldMsg()
        }
    }

    @IBOutlet var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        self.view.viewWithTag(1)?.hero.id = "loginButt"
        self.view.viewWithTag(2)?.hero.id = "forgetView"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.barTintColor = .white
    }
    func submitFunc(){
        let headers = ["Accept": "application/json",
                       
                       "Accept-Language":L102Language.currentAppleLanguage()]
        let par = ["email":"\(textField.text!)"]
        HUD.show(.progress)
        Alamofire.request(APIs.Instance.forgetPassword() , method: .post, parameters: par, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
                
                
            case .success(let value):
                 print(value)
                
                
                
                if let dict = value as? [String:AnyObject] {
                    if let _ = dict["check-the-code"] as? String {
                        let vc =  AppStoryboard.Main.viewController(viewControllerClass: secondFPVC.self)
                        vc.email = self.textField.text ?? ""
//                        vc.phone = "+971\(self.textField.text!.replacedArabicDigitsWithEnglish)"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    if let errors = dict["errors"] as? [String:AnyObject] {
                        var flag = 0
                        for value in errors.values {
                            if flag == 0 {
                                flag = 1
                                
                                if let strArr = value as? [String] {
                                    if strArr.count>0 {
                                        self.showError(msg:strArr[0], titleMsg: "error")}
                                }
                            }
                            break
                        }
                    }}
            case .failure(let error):
                print(error)
                
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
    }
    func showMissingFieldMsg(){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString("Error", comment: ""), withSubtitle: NSLocalizedString("Please fill in the missing fields", comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("OK", comment: ""), andButtons: nil)
    }
    func showError(msg:String, titleMsg:String){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: NSLocalizedString(titleMsg, comment: ""), withSubtitle: NSLocalizedString(msg, comment: ""), withCustomImage: nil, withDoneButtonTitle: NSLocalizedString("Ok", comment: ""), andButtons: nil)
        
    }
    
}
