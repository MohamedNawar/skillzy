//
//  ViewController.swift
//  GoogleAPI
//
//  Created by MACBOOK on 6/19/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import JGProgressHUD
import FCAlertView
import PulsingHalo


class MainMapVC: UIViewController , CLLocationManagerDelegate, GMSMapViewDelegate {
    var card = CardArr()
    
    @IBAction func nearbyButtPressed(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: NearByViewController.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    let hud = JGProgressHUD(style: .light)
    //    let haloView = UIView()
    //    let halo = PulsingHaloLayer()
    let marker = GMSMarker()
    var markers = [GMSMarker]()
    @IBAction func filterButtonPreessed(_ sender: Any) {
        
        let vc = AppStoryboard.Main.viewController(viewControllerClass: FilterForMapViewController.self)
        vc.filterCallBack = { (_ name:String,_ gender:String,_ company:String,_ city_id:Int,_ country_id:Int,_ position_id:Int,_ work_sector_id:Int) in
            self.getPlacesWithFilter(location_latitude: self.marker.position.latitude, location_longitude: self.marker.position.longitude, name: name, gender: gender, company: company, city_id: city_id, country_id: country_id, positionId: position_id, work_sector_id: work_sector_id)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Map".localized()
        mapView.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        
        
        
        let tempView = Bundle.main.loadNibNamed("MarkerView", owner: self, options: nil)?.first as! MarkerView
        mapView.addSubview(tempView)
        marker.iconView = tempView
        print(mapView.projection.coordinate(for: mapView.center).latitude)
        print(mapView.projection.coordinate(for: mapView.center).longitude)
        marker.position = mapView.projection.coordinate(for: view.center)
        
        marker.map = self.mapView
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let coordinate = locationManager.location?.coordinate {
            
            
            
            
            
            
            
            
            
            
            
            let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: 17.0)
            
            self.mapView?.animate(to: camera)
            //            getPlacesWithFilter(location_latitude: coordinate.latitude, location_longitude: coordinate.longitude, name: "", gender: "", company: "", city_id: -1, country_id: -1, work_sector_id: -1)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            self.locationManager.stopUpdatingLocation()
        }else {
            print("can't get location for some reason")
        }
    }
    
    
    //    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    //        print(mapView.projection.coordinate(for: mapView.center).latitude)
    //        print(mapView.projection.coordinate(for: mapView.center).longitude)
    //        marker.position = mapView.projection.coordinate(for: mapView.center)
    //    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        print(mapView.projection.coordinate(for: mapView.center).latitude)
        print(mapView.projection.coordinate(for: mapView.center).longitude)
        marker.position = mapView.projection.coordinate(for: mapView.center)
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        getPlacesWithFilter(location_latitude: marker.position.latitude, location_longitude: marker.position.longitude, name: "", gender: "", company: "", city_id: -1, country_id: -1, positionId: -1, work_sector_id: -1)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        if #available(iOS 11, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: CardInnerVC.self)
        for i in 0..<markers.count{
            if marker == markers[i]{
                vc.card = card.data?[i]
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        return true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 11, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationItem.largeTitleDisplayMode = .always
        }
        self.navigationController?.navigationBar.isHidden = true
        marker.iconView = nil
        let temp = Bundle.main.loadNibNamed("MarkerView", owner: self, options: nil)?.first as! MarkerView
        marker.iconView = temp
        marker.position = mapView.projection.coordinate(for: view.center)
    }
    
    private func setupCards(){
        mapView.clear()
        markers.removeAll()
        for i in card.data ?? []{
            let m = GMSMarker(position: CLLocationCoordinate2DMake(i.location_latitude ?? 0.0, i.location_longitude ?? 0.0))
            let img = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            img.layer.cornerRadius = 20
            img.clipsToBounds = true
            let url = URL(string: i.avatar ?? "")
            img.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "logo"), options: .progressiveDownload, completed: nil)
            m.iconView = img
            m.title = i.name
            m.map = self.mapView
            markers.append(m)
        }
        
        marker.map = self.mapView
    }
    
    
    func getPlacesWithFilter(location_latitude:Double,location_longitude:Double,name:String,gender:String,company:String,city_id:Int,country_id:Int,positionId:Int,work_sector_id:Int){
        
        //        HUD.show(.progress, onView: self.view)
        let header = APIs.Instance.getHeader()
        
        var par = "?"
        
        if location_latitude != 0.0{
            par = par + "location_latitude=\(location_latitude)&"
        }
        if location_longitude != 0.0{
            par = par + "location_longitude=\(location_longitude)&"
        }
        if name != ""{
            par = par + "name=\(name)&"
        }
        if gender != ""{
            par = par + "gender=\(gender)&"
        }
        if company != ""{
            par = par + "company=\(company)&"
        }
        if city_id != -1 && city_id != 0{
            par = par + "city_id=\(city_id)&"
        }
        if country_id != -1 && country_id != 0{
            par = par + "country_id=\(country_id)&"
        }
        if work_sector_id != -1 && work_sector_id != 0{
            par = par + "work_sector_id=\(work_sector_id)&"
        }
        
        if positionId != -1 {
            par = par + "position_id=\(positionId)"
        }
        
        print(APIs.Instance.getNearbyCards() + par)
        Alamofire.request(APIs.Instance.getNearbyCards() + par, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            
            
            
            switch(response.result) {
            case .success(let value):
                print(value)
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    do {
                        //Fail
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        self.makeDoneAlert(title: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة", SubTitle: "\(err.parseError())", Image: #imageLiteral(resourceName: "ic_koloda"))
                        
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                    }
                    
                }else{
                    //Success
                    do {
                        self.card = try JSONDecoder().decode(CardArr.self, from: response.data!)
                        self.setupCards()
                        //                                self.pares(temp: temp3)
                    }catch{
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                        self.hud.dismiss(afterDelay: 1.0)
                        
                    }
                    
                }
            case .failure(_):
                self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                self.hud.textLabel.text = "حدث خطأ برجاء اعادة المحاولة"
                self.hud.dismiss(afterDelay: 1.0)
                
                break
                
            }
        }
        
    }
    
    func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
        let alert = FCAlertView()
        alert.avoidCustomImageTint = false
        alert.colorScheme = ColorControl.sharedInstance.getMainColor()
        alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
    }
    
}
