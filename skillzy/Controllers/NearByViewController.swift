//
//  NearByViewController.swift
//  skillzy
//
//  Created by Mohamed Nawar on 8/30/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import CoreLocation
class NearByViewController: UIViewController , UITableViewDataSource , UITableViewDelegate,CLLocationManagerDelegate {
    var card = CardArr()
    let locationManager = CLLocationManager()
    var userLatitude:CLLocationDegrees! = 0
    var userLongitude:CLLocationDegrees! = 0
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return card.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CardInnerVC") as! CardInnerVC
        
        vc.card = (card.data?[indexPath.row])// ?? CardData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NearbyCell", for: indexPath)
        cell.selectionStyle = .none
        let nameLbl = cell.viewWithTag(1) as! UILabel
        let descLbl = cell.viewWithTag(2) as! UILabel
        let imgView = cell.viewWithTag(3) as! UIImageView
        let distanceLbl = cell.viewWithTag(-4) as! UILabel
        let item = card.data?[indexPath.row]
        nameLbl.text = item?.name ?? ""
        distanceLbl.text = item?.distance_formatted ?? ""
        descLbl.text = item?.position?.name ?? ""
        imgView.sd_setImage(with: URL(string: item?.avatar ?? "")) { (image, error, cache, urls) in
            if (error != nil) {
                imgView.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
            } else {
                imgView.backgroundColor = .white
                imgView.image = image
            }
            
        }
        
        
        if L102Language.currentAppleLanguage() == "ar" {
            nameLbl.textAlignment = .right
            descLbl.textAlignment = .right
        }else{
            nameLbl.textAlignment = .left
            descLbl.textAlignment = .left
        }
        return cell
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        
    }
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        getLocation()
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true

    }
    func getLocation(){
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let coordinate = locations.last?.coordinate {
            userLatitude  = coordinate.latitude
            userLongitude  = coordinate.longitude
            self.locationManager.stopUpdatingLocation()
            getNearBy()
        }
        
    }
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    func getNearBy(){
        
            HUD.show(.progress)
            let header = APIs.Instance.getHeader()
        print(APIs.Instance.getNearbyCards() + "?location_latitude=\(userLatitude ?? CLLocationDegrees())&location_longitude=\(userLongitude ?? CLLocationDegrees())")
            Alamofire.request("http://app.skillzyconnect.com/api/cards/nearby?location_latitude=\(userLatitude ?? 0)&location_longitude=\(userLongitude ?? 0)&", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
                HUD.hide()
                switch(response.result) {
                case .success(let value):
                    print(value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                    }else{
                        
                        do {
                            
                            self.card = try JSONDecoder().decode(CardArr.self, from: response.data!)
                            self.tableView.reloadData()
                        }catch{
                            print("errrr:\(error)")
                        }
                    }
                    
                case .failure( _):
                    break
                }
        }
    }

}
