//
//  SearchResultsVC.swift
//  skillzy
//
//  Created by mouhammed ali on 9/16/18.
//  Copyright © 2018 MohamedHassanNawar. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
class ReusltCell:UITableViewCell{
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var addButt: UIButton!
    var ButtonHandler:(()-> Void)!
    
    @IBAction func ButtonClicked(_ sender: UIButton) {
        self.ButtonHandler()
    }
    
}
class SearchResultsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var reesultsTable: UITableView!
    var url = ""
    var card = CardArr()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        getResults()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if card.data?[indexPath.row].user?.id == userData.Instance.data?.id {
            return 0
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return card.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell") as! ReusltCell
        cell.selectionStyle = .none
        let item = card.data?[indexPath.row]
        cell.addButt.isHidden = false
        if item?.user?.id == userData.Instance.data?.id {
            cell.addButt.isHidden = true
        }
        cell.nameLbl.text = item?.name ?? ""
        cell.descLbl.text = item?.position?.name ?? ""
        cell.imgView.sd_setImage(with: URL(string: item?.avatar ?? "")) { (image, error, cache, urls) in
            if (error != nil) {
                cell.imgView.backgroundColor = #colorLiteral(red: 0.537254902, green: 0.1058823529, blue: 0.6666666667, alpha: 1)
            } else {
                cell.imgView.backgroundColor = .white
                cell.imgView.image = image
            }
            
        }
            cell.ButtonHandler = {()-> Void in
                if let links = item?.links.value as? Links {
                    if links.remove_from_contacts?.href != nil {
                        self.addOrRemove(type:"remove", id:item?.id ?? 0)
                    }else{
                        self.addOrRemove(type:"add", id:item?.id ?? 0)
                    }
                }
        }
        if let links = item?.links.value as? Links {
            if links.remove_from_contacts?.href != nil {
                cell.addButt.setTitle("-", for: .normal)
            }else{
                cell.addButt.setTitle("+", for: .normal)
            }
        }
        if L102Language.currentAppleLanguage() == "ar" {
            cell.nameLbl.textAlignment = .right
            cell.descLbl.textAlignment = .right
        }else{
            cell.nameLbl.textAlignment = .left
            cell.descLbl.textAlignment = .left
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let vc = AppStoryboard.Main.viewController(viewControllerClass: CardInnerVC.self)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CardInnerVC") as! CardInnerVC
        
        vc.card = (card.data?[indexPath.row])// ?? CardData()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func getResults(){
        HUD.show(.progress)
        let header = APIs.Instance.getHeader()
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let value):
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    
                    do {
                        
                        self.card = try JSONDecoder().decode(CardArr.self, from: response.data!)
                        self.reesultsTable.reloadData()
                    }catch{
                        print("errrr:\(error)")
                    }
                }
                
            case .failure( _):
                break
            }
        }
    }
    func addOrRemove(type:String, id:Int){
        let url = "http://app.skillzyconnect.com/api/cards/contacts/\(id)"
        var methode = HTTPMethod.post
        if type == "remove" {
            methode = .delete
        }
        HUD.show(.progress)
        let header = APIs.Instance.getHeader()
        Alamofire.request(url, method: methode, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse) in
            HUD.hide()
            switch(response.result) {
            case .success(let _):
                
                let temp = response.response?.statusCode ?? 400
                if temp >= 300 {
                    
                }else{
                    self.getResults()
                }
                
            case .failure( _): break
                
            }
        }
        
        
    }


}
